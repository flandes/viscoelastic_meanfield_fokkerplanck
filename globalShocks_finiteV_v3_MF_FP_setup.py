from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("globalShocks_finiteV_v3_MF_FP_module", ["globalShocks_finiteV_v3_MF_FP_pyx.pyx"])]
)

