#! /bin/usr/python
## those flags have to be put before any code (excluding comments and whitespace, but no blank lines are allowed !!)
#cython: boundscheck=False 
#cython: wraparound=False 
#cython: profile=False
#cython: cdivision_warnings=False
#######################################################

#    globalShocks_finiteV_v3_MF_FP_pyx.pyx is a Cython implementation of the integration of a 2-variable, time-dependent integration scheme.
#    Started on: Jan, 2013 by F.P. Landes
#    Copyright (C) 2014 François P. Landes, Alberto Rosso, E.A. Jagla


'''
data structures:
p[j,k] is the proba. density to have     delta^R = dr[j]    and   delta^F = df[k]   .
pdeltaF[k] is the total probability of having delta^F=df[k]  (for any delta^R)
pdeltaR[j] is the total probability of having delta^R=dr[j]  (for any delta^F)

command-line arguments :
    (NN, binwidth, k0, k1, k2, Velocity, Temps, CUTOFF, fraction, Intensive_Measuring, Time2, Time3, Zero_float )
    
NN is a base multiplicator to say how big the total system should be. 1000 is a good value.
binwidth is the binning width (in units of delta)
k0, k1, k2: see article.
Velocity  : driving velocity (as compared to the relaxation time scale). If set to 0, we are in the quasi-static case (V=0^+).
Temps: unused variable. Could be used to stop the simulation after some physical time has passed.
CUTOFF: set to 0.01, by dfault. This says how long the avalanche should last in terms of remaining unstable sites, before we consider that it is over.
fraction: should be set to 1. Represents the "number" of steps that one goes forward when there is the drive. Basiclly, fraction = Ve0*epsilont / binwidth.
Intensive_Measuring : to be activated (set to 1 or True or anything else than 0) in order to record the stress, etc. at all times. It is only useful to make simple plots of the system along time. Commonly set to 0 to increase performance. (we typically only need to know about the final state of the system).
Time2: if set to Time2>0, represents the time at which one wants to stop the driving. Needed for the slide-hold-slide protocol and only in that case.
Time3: similar to Time2, used to set the time at which to resume driving after it has been stopped. To be set to 0 when we don't do the slide-hold-slide protocol.
Zero_float: small float that indicates how much successive values of the stress measured should be close, before we consider them equal. A minimum decent value is 1e-2, but 1e-4 is more secure. For example, to obtain a precision of 11e-5 on the stationary stress in the steady-sliding regime, one needs to set Zero_float to 1e-5 or less.

NOTE: 
most of the code consists in the definition of the initial state and output files, and in the wrtings to the output files.
Only a small fraction is actually the integration, and the rest consists in the checks made to detect that we reached convergence.

'''




import sys, time, os

# "cimport" is used to import special compile-time information
# about the numpy module (this is stored in a file numpy.pxd which is
# currently part of the Cython distribution).
import numpy as np
cimport numpy as np   # this is how it's done at http://docs.cython.org/src/userguide/numpy_tutorial.html
cimport cython
from libc.math cimport exp        ## this is the exp() function from the C library "math.h" --- #include <math.h> is available.
#include <iostream>

### CONSTANTS, INLINED in the code at compilation time (the most efficient way possible)  ###
#DEF Epsilon_Machine=1e-7

# We now need to set a data type for our arrays. I've used the variable  DTYPE for this, which is assigned to the usual NumPy runtime type info object.
DTYPE32 = np.int32
DTYPE64 = np.int64
DTYPEuint8 = np.uint8
DTYPEint32 = np.int32
# "ctypedef" assigns a corresponding compile-time type to DTYPE_t. For every type in the numpy module there's a corresponding compile-time type with a _t-suffix.
ctypedef np.int32_t DTYPE_int32_t  #  int32 is necessary or single - index arrays, those that can go theoretically up to Lx.Ly
ctypedef np.int64_t DTYPE_int64_t  #  int32 is necessary or single - index arrays, those that can go theoretically up to Lx.Ly
ctypedef np.float32_t DTYPE_f32_t  #   about 7 secured digits: enough for us
ctypedef np.float64_t DTYPE_f64_t  #   about ?? secured digits: a lot !


## choices of precision:
DTYPEF = np.float32
ctypedef np.float32_t DTYPE_f_t
DEF PRECISION = "f32"

DTYPEint = np.int
ctypedef np.int_t DTYPE_int_t  

## arguments passed via the command-line 
(programFileName, NSTR, binwidthSTR, TempsSTR, DisorderType, k0STR, k1STR, k2STR, VelocitySTR, CUTOFFSTR, fractionSTR, Intensive_MeasuringSTR, Time2STR, Time3STR, Zero_floatSTR ) = sys.argv
if float(VelocitySTR) != 0.0 :
    TempsSTR = str(round(float(TempsSTR)/float(VelocitySTR) , 3) )
else : 
    TempsSTR = str(round(float(TempsSTR)/float(0.001) , 3) )


## 3 small functions ##
def isnormalized(ditrib, binwidth):
    '''
    Computes and return the integral of the distribution P(deltaF, deltaR), which should be equal to 1, always. 
    '''
    norm = np.sum(ditrib)*binwidth
    return norm
    
def Pdt_func(p,NN):
    '''
    Computes the P(delta) function (vector), from the P(deltaF, deltaR) function (matrix).
    '''
    Pdt = np.zeros(NN)
    (i_array,j_array) = np.where(p>0.)  ## it is enough to compute this complexe sum for the non-zero elements of p.
    for index in xrange(len(i_array)):
        Pdt[min(i_array[index] + j_array[index] - NN/2, NN-1)] += p[i_array[index] , j_array[index]]
    return Pdt 

def approx_egal(x1,x2,tolerance):
    '''
    Checks if the relative difference between two scalar values is smaller than some threshold, "tolerance".    
    '''
    if abs( (x1-x2)/min(x1,x2) ) < tolerance :
        return 1
    else:
        return 0
        


def globalShocks_finiteV_v3_MF_FP_main():
    '''
    The "main" function. It stops when the integration is over, i.e. when the stress has stabilized to a constant value, or when the Global Shocks (GS) have stabilized to a constant stress drop (=stress gap). 
    '''
    
    ## declaration of the variables ##
    cdef :
        DTYPE_int64_t NN
        DTYPE_int64_t i,j,k, kcurrent, int_shift,  step_number, Nsteps, last_non_zero_element_of_g, avalancheSteps, nLimit, measure_of_pdelta_after_the_avalanches, DriveTag
        DTYPE_f64_t binwidth, k0, k1, k2, barz, etau, Velocity, V, epsilont
        DTYPE_f64_t P00, P00p1, goal_k, k2overK, meanDeltaF, shi, float_shift, relaxation_factor_over_binwidth, ti, Temps,  OldNorm, NewNorm, P00p2, sigma_square, std_dev_sigma,  std_dev_sigma_old, new_std_dev_sigma,  widthOfStressGap, fraction, Vinitial, Intensive_Measuring, Time2, Time3, critical_P00, new_meanDeltaF,  Zero_float, P00effectif, sumP0, CUTOFF, barzk1k2, redrive, redrive_old, P00previous, P00Gaps

    ########################################################################
    # converting command-line arguments (strings) into integers and floats #
    (NN, binwidth, k0, k1, k2, Velocity, Temps, CUTOFF, fraction, Intensive_Measuring, Time2, Time3, Zero_float ) = int(NSTR), float(binwidthSTR), float(k0STR), float(k1STR), float(k2STR), float(VelocitySTR), float(TempsSTR), float(CUTOFFSTR), float(fractionSTR), float(Intensive_MeasuringSTR), float(Time2STR),float(Time3STR), float(Zero_floatSTR)


    ############################################################################################################################################
    ## one may set the intial value of the ratio "fraction" to something else than 1, but the behvaior in that case is not controlled. ##
    if fraction < 1 :
        print 'One may set the intial value of the ratio "fraction" to something else than 1, but the behvaior in that case is not controlled (!!).\nExiting now.'
        raise SystemExit
    if fraction > 1 :
        print '" fraction > 1 " does not make sense.\nExiting now.'
        raise SystemExit
        
        
    
    ##########################################################################
    ## CONSTANTS of the program (should not need to be explicitly modified) ##
    measure_of_pdelta_after_the_avalanches = 1
    nLimit = 20       ## minimal number of GS observed before it is allowed to exit the integration ##
    barz = 0.1          ## value of \overline{z}.     
    ## formula for the number of bins used in the discretization of the matrix p.
    NN = int(0.005/binwidth*NN*(k0+k1+k2)/1.0*barz/0.1) + 200     ## the NN given in the command line is used here. 
    if NN%2 != 0 :   # currently, the algorithm only supports even numbers for the value of NN.
        NN+=1
    print 'NN, binwidth, Temps,  DisorderType       k0, k1, k2,       Velocity     Intensive_Measuring?     Time2      Time3    Zf'
    print NN, binwidth,'  ', Temps, '     ', DisorderType, '          ',    k0, k1, k2, '         ', Velocity,  '  ',  Intensive_Measuring, '                         ', Time2,'   ', Time3, '   ', Zero_float, '\n'
    etau = 1.0
    Ve0=Velocity
    if Ve0 > 0.0 :  
        Vinitial = Ve0
    else: 
        Vinitial = 0.0 ## quasi-static case, where the driving is infinitely slow compared to relaxation. 
                       ## We still need a finite Ve0 for the passage of time and definition of epsilont 
        print 'Vinitial = 0. We use '
        Ve0 = 0.00001
    ## practical constants (save a bit of cimputation time) ##
    K=(k0+k1+k2)
    Kbarz = K*barz
    k2overK = k2/(k0+k1+k2)
    critical_P00 = 1.0 /(barz*(k1+k2))
    barzk1k2 = barz*(k1+k2)
    # remember that basically, fraction==1 
    epsilont = binwidth / (k0*Ve0) * fraction
    relaxation_factor_over_binwidth  = (1.0-np.exp(- epsilont  * k2/etau)) / binwidth
    print 'relaxation_factor=', relaxation_factor_over_binwidth*binwidth
    if Vinitial ==0:    ## quasi-static case (see above) ##
        relaxation_factor_over_binwidth = 1.0/binwidth
    OldNorm=1.0
    NewNorm=0.0
    P00=0.
    P00p1=0.
    P00p2=0.
    new_meanDeltaF=0.0
    Nsteps = 3      ## number of steps between two recordings (recordings+checks of stationarity) ##
    if Nsteps %2 ==0 :
        Nsteps +=1 # we want this to be an odd number, to avoid ill-defined 2-step oscillations to be considered as a "stationnary" solution.
    ti = 0.
    std_dev_sigma = -1.0
    std_dev_sigma_old = -1.0
    step_number=0
    AverageStressList=[]   
    widthOfStressGaplist=[]
    timeOfGapslist=[]
    widthOfStressGap=0.0
    widthOfStdDevStressGap=0.0
    if nLimit <= 3: 
        nLimit =4

    ##############################################################################################
    ## declaration of arrays (this is what makes the Cython  ~200 times faster than the python) ##
    # structure of p: the lines are at equal delta^R,  a given column has an equal delta^F.
    cdef np.ndarray[DTYPE_f_t, ndim=2] p      = np.zeros([NN, NN], dtype=DTYPEF)
    cdef np.ndarray[DTYPE_f_t, ndim=1] pnew_k = np.zeros(NN  , dtype= DTYPEF)
    cdef np.ndarray[DTYPE_f_t, ndim=1] pdeltaF    = np.zeros(NN  , dtype= DTYPEF)
    cdef np.ndarray[DTYPE_f_t, ndim=1] P0     = np.zeros(NN  , dtype= DTYPEF)
    cdef np.ndarray[DTYPE_f_t, ndim=1] g1_array= np.zeros(2*NN, dtype= DTYPEF)
    # df, dr and dc are arrays which represent e.g. the value of delta^F at a given index.
    cdef np.ndarray[DTYPE_f_t, ndim=1] df     = np.arange(-NN/2, NN/2, dtype=DTYPEF)*binwidth
    cdef np.ndarray[DTYPE_f_t, ndim=1] dr     = np.arange(-NN/2, NN/2, dtype=DTYPEF)*binwidth
    cdef np.ndarray[DTYPE_f_t, ndim=1] dt     = np.arange(-NN/2, NN/2, dtype=DTYPEF)*binwidth

    #############################################################################################################
    ## BUILDING the "function" g(z). It is actually computed once and for all, and stored in a vector g1_array ##
    ## precisely, g1(z) = g(z/K)/K. ##
    if DisorderType == 'expo':
        ArtificialCut_Off = int(Kbarz*15/binwidth)
        g1_array[NN+1:int(NN+1+1+ArtificialCut_Off)]= np.array([np.exp( -(i)*binwidth/Kbarz )/Kbarz for i in range(ArtificialCut_Off+1) ])
    if DisorderType == 'uniform':
        g1_array[int(NN+1+Kbarz*0.5/binwidth):int(NN+1+Kbarz*1.5/binwidth)] = 1./Kbarz
    g1_array /= np.sum(g1_array)    
##    a=np.where(g1_array>0)[0]
##    last_non_zero_element_of_g = a[len(a)-1]
##    del a
##    print 'last_non_zero_element_of_g ==', last_non_zero_element_of_g, ' should be much smaller than NN=', NN


    ######################################
    ### BUILDING THE INITIAL CONDITION ###

    ## construction of p in the deltaF direction ##
    pdeltaF = g1_array[NN/2:3*NN/2]
    pdeltaF[df<0] = 0.
    pdeltaF /= isnormalized(pdeltaF, binwidth)
    p[NN/2, :] = pdeltaF  # INITIAL CONDITION BY DEFAULT : with deltaR=0 everywhere: #
    pdeltaF = np.sum(p,0)  
    meanDeltaF = np.sum(pdeltaF*df)*binwidth
    
    ## construction of p in the deltaR direction ##
    ### three different choices of initial condition are possible:      ###
    ### either fully relaxed, partially relaxed, or not relaxed at all. ###
#    relaxation_factor = 1.0
#    common_name = '_ri=1'
#    relaxation_factor = relaxation_factor_over_binwidth * binwidth
#    common_name = '_ri=0.5'
    relaxation_factor = 0.0
    common_name = '_ri=0'

    for k in range(NN) :
        goal_k = (meanDeltaF - df[k])*k2overK 
        for j in range(NN-10):
            shi = (goal_k - dr[j]) * relaxation_factor / binwidth
            int_shift = int(shi) 
            if shi < 0.0:
                int_shift-=1
            float_shift = shi - int_shift     #between 0 and 1
            if j+int_shift >= NN-2:
                print 'insufficient matrix width1'
                int_shift =NN-2-j
            if j+int_shift < 0:  
                int_shift = -j
                print 'insufficient matrix width2'
            pnew_k[j+int_shift] += p[j,k]*(1.0-float_shift) 
            pnew_k[j+int_shift+1] += p[j,k]*float_shift
        for j in range(NN):
            p[j,k]=pnew_k[j] #* Norm
            pnew_k[j] = 0.
    p /= isnormalized(p, binwidth)

    ## we rigidly shift the distribution p to the left, along the deltaF axis, as long as the element P(deltaF=0) is 0. ##
    pdeltaF = np.sum(p,0)
    meanDeltaF = np.sum(pdeltaF*df)*binwidth
    df_gap = np.where(pdeltaF >0)[0][0]
    dt_gap = df_gap + (k2*(meanDeltaF-df[df_gap])/(k0+k1+k2))//binwidth -NN/2
    if dt_gap > 2: 
        n_shift = int(dt_gap) - 2
        for i in range(NN):
            p[i]=np.roll(p[i], -n_shift)

    ## we compute the average deltaR. It should be close to 0 ##
    pdeltaR = np.sum(p,1)
    print '<delta R> ==', np.sum(pdeltaR * dr) * binwidth , ' == 0 ? '
    if np.sum(pdeltaR * dr) * binwidth > 1e-2 :
        print 'the average delta R is too far from 0.\nExiting now.'
        raise SystemExit







    #############################################################################
    ######################## OUTPUTS #############################################

    ## trying to make a new directory, to put output files in it in a tidy way. ##
    try:
        os.mkdir('streams/')
    except OSError:
        pass

    ## names of the output files ##
    common_name='streams/dat-gloShocks_finiteV_v3_MF_FP_pyx_k0='+k0STR+'_k1='+k1STR+'_k2='+k2STR+'_V='+VelocitySTR+'_T2='+Time2STR+'_T3='+Time3STR+'_cut='+CUTOFFSTR+'_frac='+fractionSTR+'_bnw='+binwidthSTR+'_NN='+str(NN)+'_Zf='+Zero_floatSTR+common_name
    output_file1_name = common_name+'_type=scalarsList_'
    output_file2_name = common_name+'_type=partialpMatriX'
    if Vinitial ==0: 
        GSName = 'GS_GlobalContainer_V=0_'
    else:
        GSName = 'GS_GlobalContainer_k0='+k0STR
    GSName_bonus = '_k1='+k1STR+'_k2='+k2STR+'_cut='+CUTOFFSTR+'_frac='+fractionSTR+'_bnw='+binwidthSTR+'_NN='+str(NN)+'_Zf='+Zero_floatSTR+'_n'+str(nLimit)
    GSName += GSName_bonus
    print GSName

    ## opening some streams (files), to write into them ##
    fileSingles=open(output_file1_name, 'w') #just to overwrite the previous file if it exists.
    fileSingles.close()
    fileSingles6=open(common_name+'_beforeRelaxation_stream', 'w')
    fileSingles6.close()
    fileSingles6=open(common_name+'_afterRelaxation_stream', 'w')
    fileSingles6.close()
#    print ("replot '"+common_name+"_stream' u 1:2 w l")
#    print ("replot '"+common_name+"_postRelaxation_stream' u 1:2 w l")
#    print ("replot '"+common_name+"_stream' u 1:(1-$4) w l")
#    print ("replot '"+common_name+"_postRelaxation_stream' u 1:(1-$4) w l")
    
    if measure_of_pdelta_after_the_avalanches == 1:
        ## wrting the P(delta), P(deltaF) and P(deltaR) vectors down to a file. ##
        fileSingles2=open(output_file2_name+'initialState', 'w')
        Pdt = Pdt_func(p,NN)
        pdeltaF = np.sum(p,0)  
        pdeltaR = np.sum(p,1)
        for i in range(NN):
            fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')
        fileSingles2.close()

    
    
    t0 =time.time()
    main_exit = False
    ###########################################################################################################################
    ################################################ MAIN LOOP ################################################################
    ### we perform until we converged, or reach a crazily long time. (there are two ways to converge).
    while main_exit  == False:
        step_number += 1


        ##################################################################################################
        ## this changes the driving velocity (to 0) only between the times Time2 and Time3, if Time2>0. ##
        DriveTag = True
        if ti >= Time2 and ti <= Time3 and Time2>0.0:
            DriveTag = False            

        ########################################################
        ## some recording, and checks to see if we converged. ##
        if step_number % Nsteps ==0:
            fileSingles=open(output_file1_name, 'a')
            fileSingles.write(str(ti)+' '+str(P00)+' '+str(P00p1)+' '+str(meanDeltaF)+' '+str(std_dev_sigma)+ ' '+str(widthOfStressGap)+ ' \n')
            fileSingles.close()
            print ti, 1-meanDeltaF, P00, P00p1, widthOfStressGap
            if P00 > 1e-7:  # we only check things when P(0) > 0.
                AverageStressList.append(meanDeltaF)
                n=len(AverageStressList)
                if n>=3:    ## we need to compare at least three values.
                    if approx_egal( std_dev_sigma, std_dev_sigma_old, Zero_float):
                        if approx_egal( AverageStressList[n-1], AverageStressList[n-2], Zero_float):
                            if approx_egal( AverageStressList[n-1], AverageStressList[n-3], Zero_float):
                                if ti > Time3:
                                    main_exit = True   # breaking the WHILE loop.
                                    print 'Exiting because the stress (or the "meanDeltaF" variable) no longer changes. \nWe probably reached a stationary state.'
                                    widthOfStressGap = 0.0
                                    widthOfStdDevStressGap = 0.0
                    else: 
                        if step_number > Nsteps *10 *100*10:
                            print 'time is step_number=',step_number, ' and ti=', ti,  '  .... this meanDeltaFs that we will probably never converge. The integration did NOT converge,'
                            print 'so we write invalid outputs to files, and \nWE EXIT NOW !!!'
                            widthOfStressGap = -42.0
                            main_exit = True
                    std_dev_sigma_old = std_dev_sigma

                    if step_number % (Nsteps *10*10) ==0:  
                        ## wrting the P(delta), P(deltaF) and P(deltaR) vectors down to a file. ##
                        fileSingles2=open(output_file2_name+'NOT_YET_CONVERGED', 'w')
                        Pdt = Pdt_func(p,NN)
                        pdeltaF = np.sum(p,0)  
                        pdeltaR = np.sum(p,1)
                        for i in range(NN):
                            fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')  ## to be build
                        fileSingles2.close() 
                        print 'output written to a file (NOT_YET_CONVERGED)'
        ################## end of recording ####################
        ########################################################
        
        
        
        ##################################################################################
        ################# AVALANCHE PROCESS ##############################################
        ####### INITIAL DRIVE of one bin, followed y the redrive due to the jumps ########
        avalancheSteps = 0
        if DriveTag == True :
            # DRIVE (of one binning width) 
            for j in range(2,NN-2) : ## loop on the deltaR's ##
                for k in range(0, NN-1) : 
                    p[j,k] =  p[j,k+1]
            # KILL (smooth)
            sumP0 = 0.
            for j in range(2,NN-2):
                P0[j]=0.
                for kcurrent in range(NN-j-1,0,-1):
                    P0[j] += p[j,kcurrent]      # p[j,NN-j-1] is included, but p[j,NN-j] is excluded, i.e. what counts P00 does not count P0
                    p[j,kcurrent] = 0.
                sumP0 += P0[j]
            # JUMP
            for j in range(2,NN-2) : 
                for k in range(NN-j-1,NN): # we don't need to explore the k+j < NN because g1 is zero there.
                    p[j,k] += P0[j]*g1_array[k+j]*binwidth

            P00=0.
            for j in range(2,NN-2):
                P00 += p[j,NN-j]


            redrive_old=1.0     # this is indicated as the "r" variable in the article. #   
            ###########################################################################################################
            ### back-drive: due to the jumps, there is an equivalent driving.  (j>1 in the proba. density function) ###
            while sumP0 > CUTOFF * critical_P00 or P00 >= critical_P00 :
                redrive = min(sumP0 * barzk1k2, 1.0)
                if P00 >= critical_P00 :
                    redrive =1.0
                if redrive >= redrive_old :     # we count the number of steps in which the avalanche' size grows.
                    avalancheSteps +=1      
                redrive_old = redrive    
                
                # DRIVE (smooth) 
                # p is driven along the deltaF axis, of only a fraction "redrive" of the binning length, i.e. it is smoothly driven, 
                for j in range(2,NN-2) : ## loop on the deltaR's ##
                    for k in range(0, NN-1) :
                        p[j,k] +=  redrive * ( p[j,k+1] - p[j,k] )      
                # KILL (smooth)
                sumP0 = 0.
                for j in range(2,NN-2):
                    P0[j]=0.
                    for kcurrent in range(NN-j-1,0,-1):
                        P0[j] += p[j,kcurrent]      # p[j,NN-j-1] is included, but p[j,NN-j] is excluded, i.e. what counts P00 does not count P0
                        p[j,kcurrent] = 0.
                    sumP0+=P0[j]
                # JUMP
                for j in range(2,NN-2) : 
                    for k in range(NN-j-1,NN): # we don't need to explore the k+j < NN because g1 is zero there.
                        p[j,k] += P0[j]*g1_array[k+j]*binwidth
                P00=0.
                for j in range(2,NN-2):
                      P00 += p[j,NN-j]

        ####
        ### END OF THE DRIVE+JUMPS process (Avalanche process) ###
        ####


        ####################################################################################################
        ## in all cases, we need to update pdeltaF for what follows (after an avalanche, it has changed!) ##        
        for k in range(2,NN-2) : # going through the delta^F
            pdeltaF[k]=0.
            # going through the delta^R such that   delta = deltaF + deltaR > 0   (these are the only ones where p[j,k]>0 ). #
            for j in range(NN-k-1, NN) : 
                pdeltaF[k] += p[j,k]
        
        ##################################################################################
        ## IF there was a non-trivial avalanche, with more than a single step of growth ##
        ## THEN we consider that there was a "global shock" (GS)                        ##
        if avalancheSteps > 1 :
            new_meanDeltaF=0.
            sigma_square = 0.
            for k in range(NN):
                new_meanDeltaF += pdeltaF[k]*df[k]
                sigma_square += pdeltaF[k] * (1.0- df[k]*binwidth)**2
            new_meanDeltaF *= binwidth
            new_std_dev_sigma = ( sigma_square - (1.0-new_meanDeltaF)**2 )**0.5
            widthOfStressGap = new_meanDeltaF - meanDeltaF 
            widthOfStressGaplist.append(widthOfStressGap)
            widthOfStdDevStressGap = std_dev_sigma - new_std_dev_sigma
            P00Gaps = P00previous - P00
            timeOfGapslist.append(ti)            
            n=len(widthOfStressGaplist)
            if n >= nLimit:
                if approx_egal( widthOfStressGaplist[n-1], widthOfStressGaplist[n-2], 0.001):
                    if approx_egal( widthOfStressGaplist[n-1], widthOfStressGaplist[n-3], 0.001):  
                        main_exit = True   # breaking the WHILE 
                        print 'Exiting because widthOfStressGap no longer changes.'
                        print 'We have probably reached a "stationary" regime, in the sense that the system has a well-defined periodic behavior, with the stress drop being equal three times in a row.'
                        print 'We have converged.'

            if Intensive_Measuring == True and measure_of_pdelta_after_the_avalanches == 1 :
                # wrting the P(delta), P(deltaF) and P(deltaR) vectors down to a file. ##
                fileSingles2=open(output_file2_name+'afterAvalanche_'+str(ti), 'w')
                Pdt = Pdt_func(p,NN)
                pdeltaF = np.sum(p,0)  
                pdeltaR = np.sum(p,1)
                for i in range(NN):
                    fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')  ## to be build
                fileSingles2.close() 
                print 'Output written to a file (AFTER an Avalanche, BEFORE the relaxation that will follow).'
        P00previous=P00 # this is intentionally out of the loop: we want to consider the gaps in P00 just before/after a single avalanche #
        
        ###############################################################
        ####### computing the stress, \sigma = 1 - <\delta>  ##########
        meanDeltaF = 0.       ## == <\delta>
        sigma_square = 0.
        for k in range(NN):
            meanDeltaF += pdeltaF[k]*df[k]
            sigma_square += pdeltaF[k] * (1.0- df[k]*binwidth)**2
        meanDeltaF *= binwidth
        std_dev_sigma = ( sigma_square - (1.0-meanDeltaF)**2 )**0.5
        
        NewNorm = 0.                 
        for j in range(NN):
            for k in range(NN) :
                NewNorm += p[j,k]
        OldNorm = 1.0/(NewNorm*binwidth)
        for j in range(NN): 
            for k in range(NN) :
                p[j,k] *= OldNorm    
                
        ###############################################################
        ####################### MOVIES !! #############################
        ## uncomment to get the full movie of the integration, i.e. to see the evolution of P(delta) during the integration.
        ## one should copy-paste the screen output to a gnuplot terminal, or use python to produce the plots/movies.

        ## selecting a time window ##
#        if ti > 1 and ti < 1.2:  # python globalShocks_finiteV_v3_MF_FP_run.py 1000 0.005  1.0 expo    0.01 0.1 0.9 0.11    92000  0.05  0.0 0.0  1e-2
#            
#            print 'wrtiting     \nreplot \''+output_file2_name+'allTimes_'+str(ti)+  '\' u 0:1 w l'
#            fileSingles2=open(output_file2_name+'allTimes_'+str(ti), 'w')
#            Pdt = Pdt_func(p,NN)
#            pdeltaF = np.sum(p,0)  
#            pdeltaR = np.sum(p,1)
#            for i in range(NN):
#                fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')
#            fileSingles2.close()
#            time.sleep(1.)

        ##################################################################################
        ######################### SOME OUTPUT WRITING (before relax.) ####################
        if step_number %100 == 0 or Intensive_Measuring == True :
            P00=0.
            P00p1=0.
            P00p2=0.
            for j in range(3,NN-3):
              P00   += p[j,NN-j]
              P00p1 += p[j,NN-j+1]
              P00p2 += p[j,NN-j+2]
            fileSingles6=open(common_name+'_beforeRelaxation_stream', 'a')
            fileSingles6.write(str(ti)+' '+str(P00)+' '+str(P00p1)+' '+str(meanDeltaF)+' '+str(std_dev_sigma)+ ' '+str(widthOfStressGap)+ ' '+str(P00p2)+ ' '+str(ti*Ve0)+ ' \n')
            fileSingles6.close()
            if step_number %100 == 0 and measure_of_pdelta_after_the_avalanches == 1 :
                fileSingles2=open(output_file2_name+'beforeRelaxation_'+str(ti), 'w')
                Pdt = Pdt_func(p,NN)
                pdeltaF = np.sum(p,0)  
                pdeltaR = np.sum(p,1)
                for i in range(NN):
                    fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')
                fileSingles2.close() 
                print 'output written to a file (BEFORE relaxation occcured)'



        ########################################################################################
        ##################################### Relaxation #######################################
        for k in range(NN) :
            goal_k = (meanDeltaF - df[k])*k2overK
            for j in range(NN-10):
                shi = (goal_k - dr[j]) * relaxation_factor_over_binwidth
                int_shift = int(shi)
                if shi < 0.0: 
                    int_shift -= 1
                float_shift = shi - int_shift     #between 0 and 1
## uncomment only to debug. ##
#                if j+int_shift >= NN-2:
#                    print 'insufficient matrix width 1'
#                    int_shift =NN-2-j
#                if j+int_shift < 0:  
#                    int_shift = -j
#                    print 'insufficient matrix width 2'
                pnew_k[j+int_shift]   += p[j,k]*(1.0-float_shift)
                pnew_k[j+int_shift+1] += p[j,k]*float_shift
                
            for j in range(NN):
                p[j,k]=pnew_k[j] #* Norm
                pnew_k[j] = 0.
        # end of the k loop #

        ####
        ### END OF THE RELAXATION process ###
        ####



        ##################################################################################
        ######################### SOME OUTPUT WRITING ( after relax.) ####################
        if step_number %100 == 0 or Intensive_Measuring == True:
            P00=0.
            P00p1=0.
            P00p2=0.
            for j in range(3,NN-3):
              P00   += p[j,NN-j]
              P00p1 += p[j,NN-j+1]
              P00p2 += p[j,NN-j+2]
            fileSingles6=open(common_name+'_afterRelaxation_stream', 'a')
            fileSingles6.write(str(ti)+' '+str(P00)+' '+str(P00p1)+' '+str(meanDeltaF)+' '+str(std_dev_sigma)+ ' '+str(widthOfStressGap)+ ' '+str(P00p2)+ ' '+str(ti*Ve0)+ ' \n')
            fileSingles6.close()
            if step_number %100 == 0 and measure_of_pdelta_after_the_avalanches == 1 :
                fileSingles2=open(output_file2_name+'afterRelaxation_'+str(ti), 'w')
                Pdt = Pdt_func(p,NN)
                pdeltaF = np.sum(p,0)  
                pdeltaR = np.sum(p,1)
                for i in range(NN):
                    fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')
                fileSingles2.close() 
                print 'output written to a file (AFTER relaxation occcured)'


        ###############################################################
        ####################### MOVIES !! #############################
        ## uncomment to get the full movie of the integration, i.e. to see the evolution of P(delta) during the integration.
        ## one should copy-paste the screen output to a gnuplot terminal, or use python to produce the plots/movies.
#        if ti > 1 and ti < 1.2 :  # python globalShocks_finiteV_v3_MF_FP_run.py 1000 0.005  1.0 expo    0.01 0.1 0.9 0.11    92000  0.05  0.0 0.0  1e-2
#            print 'wrtiting     \nreplot \''+output_file2_name+'allTimes_'+str(ti)+'DOS'+  '\' u 0:1 w l'
#            fileSingles2=open(output_file2_name+'allTimes_'+str(ti)+'DOS', 'w')
#            Pdt = Pdt_func(p,NN)
#            pdeltaF = np.sum(p,0)  
#            pdeltaR = np.sum(p,1)
#            for i in range(NN):
#                fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' \n')
#            fileSingles2.close()
#            time.sleep(1.)

        ti += epsilont 
        
        ######################################## END OF  MAIN LOOP ################################################################
        ###########################################################################################################################

    print 'The main simulation loop is over.'

    ## computing the period of the oscillations, in the case where there are perioidc global shocks (GS).
    OscillationPeriod =0
    timesDiffs = np.zeros(NN)
    if len(timeOfGapslist)>1:
        timesDiffs[0:len(timeOfGapslist)-1] = np.diff(timeOfGapslist)
        OscillationPeriod = timesDiffs[len(timeOfGapslist)-2]
    # wrting the P(delta), P(deltaF) and P(deltaR) vectors down to a file, along with the time periods observed between Globals Shocks (GS).
    Pdt = Pdt_func(p,NN)
    pdeltaF = np.sum(p,0)  
    pdeltaR = np.sum(p,1)
    fileSingles2=open(output_file2_name+'finalState_ti='+str(ti), 'w')
    for i in range(NN):
        fileSingles2.write(str(Pdt[i])+' '+str(pdeltaF[i])+' '+str(pdeltaR[i])+' '+str(timesDiffs[i])+' \n') 
    fileSingles2.close() 
    
    
    ## writing of a lot fo information into a very dense meta-output file.
    # these files that start with "GS" should be aggregated, each line of such a file constitutes a point in a graph #
    fileSingles7=open(GSName, 'a')
    n=len(AverageStressList)
    fileSingles7.write(str(P00p2)+' '+str(P00)+' '+str(P00p1)+'    '+str(Vinitial)+'   '+str(k0)+'    '+str(k1)+' '+str(k2)+' '+str(binwidth)+' '+str(NN)+' '+str(Zero_float)+' '+str(nLimit)+' '+str(std_dev_sigma)+ ' '+str(widthOfStressGap)+ ' '+str(CUTOFF)+ ' '+str(fraction)+' '+str(1-meanDeltaF)+' '+str(widthOfStdDevStressGap)+' '+str(OscillationPeriod)+' '+str(np.round(time.time()-t0)) +' '+str(P00Gaps+P00)+' \n')
    fileSingles7.close()

    ## some output to the screen (redundant with the writings to files) ##
    print P00
    print P00p1
    print widthOfStressGaplist
    print widthOfStressGap
    print timeOfGapslist, ti
    print meanDeltaF
    print new_meanDeltaF

#end of file#



#EOF
