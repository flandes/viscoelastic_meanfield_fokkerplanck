
===================================================
Requirements:

    python
    cython
    g++ or some c compiler (see Cython)

===================================================
to compile, type in the terminal:

    python globalShocks_finiteV_v3_MF_FP_setup.py build_ext --inplace

This will cythonize the code (call cython to trnasform the .pyx code into .c code) 
and then compile this code with the appropraite flags, producing a python module (.so extension). 
The .so extension is called by the program  "..._run.py"

to check that it runs:
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.008 1.0 expo 0.01 0.1 0.9 10000 0.01 1.0 0 0 0  0.001  

==================================================
*A few example commands are:*

produce a stationary state:
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 10000 0.01 1.0 0 0 0  0.001  


produce global shoks:
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 0.001 0.01 1.0 0 0 0  0.001  


To produce slide-hold-slide kind of figures:
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 1000.0  0.01 1.0 1 0.005 0.005003  0.0001 
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 1000.0  0.01 1.0 1 0.005 0.00503   0.0001  
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 1000.0  0.01 1.0 1 0.005 0.0051    0.0001  
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 1000.0  0.01 1.0 1 0.005 0.006     0.0001  
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 1000.0  0.01 1.0 1 0.005 0.015     0.0001  
    python globalShocks_finiteV_v3_MF_FP_run.py  1000 0.001 1.0 expo 0.01 0.1 0.9 1000.0  0.01 1.0 1 0.005 0.0050003 0.0001 



===================================================
