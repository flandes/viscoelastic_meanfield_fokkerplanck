*Cython viscoElastic_MeanField_FokkerPlanck integration scheme*  

globalShocks_finiteV_v3_MF_FP_pyx.pyx is a Cython implementation of the integration of a 2-variable, time-dependent integration scheme of a Fokker-Planck equation derived for the mean field version of a model of visco-elastic interface pulled in a random medium.
Started on: Jan, 2014 by F.P. Landes
Copyright (C) 2014 François P. Landes, Alberto Rosso, E.A. Jagla

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

Additional information available in:
https://www.dropbox.com/s/kq6ocuk5f3xqrnu/complements_to_the_SM-cut.pdf?dl=0

This code was originally implemented for: "Frictional dynamics of viscoelastic solids driven on a rough surface",  
François P. Landes, Alberto Rosso, E.A. Jagla
Phys. Rev. E 92, 012407
http://arxiv.org/abs/1503.07847
**Please cite when appropriate**.  
